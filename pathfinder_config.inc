<?php

/*
 * @file
 * PathFinder - Short urls and future-proof url aliases.
 * Designed and developed by netgenius.co.uk.
 * Contact for commercial suppport and customisation: http://netgenius.co.uk/pathfinder
 */

/*
 * Simple function to get config variables.  Returns $vars array.
 * @todo Config forms, variables module support.
 */
 function pathfinder_load_config() {
  // Define our variables and their default values...
  $vars = array(
    // Prefix/suffix strings for url building.
    'fixes' => array('', '/'),
    // Formats are currently: simple, numeric, packed.
    'format' => 'packed',
    // Dictionary for 'packed' format. We omit all vowels to avoid generation of dictionary words.
    'dict_str' => 'bcdfghjklmnpqrstvwxzBCDFGHJKLMNPQRSTVWXZ0123456789',
    // Whether to redirect a request to current url if different - used by PathFinder Redirect.
    'redirect' => TRUE,
    // Entity types and internal types that we handle.
    // Note, 'simple' format uses the keys given here to build an alias (e.g. n123, u1, t9 etc.)
    'types' => array(
      'n' => 'node',
      'c' => 'comment',
      't' => 'taxonomy_term',
      'u' => 'user',
       // Allow for further expansion (aliases, etc.)
      'x' => 'xpanded',
    ),

    // Whether to use Pathauto (if installed) to generate aliases.
    'alias_use_pathauto' => TRUE,
    // Whether to use standard Drupal aliases.
    'alias_use_standard' => TRUE,
    // Whether to override the standard alias. If FALSE we won't process any path which already has a defined alias.
    'alias_override' => TRUE,
    // Whether to override the front-page path with an empty path.
    'frontpage_override' => TRUE,
    // Whether to convert incoming comment/123 to comment/123#comment-123 (not yet implemented).
    'comment_fragment' => TRUE,
    // Turn off if url_inbound_alter hook is not to be run (e.g. to support some other module.)
    // If configured to use paths like node/1234-anything-here then inbound procesing can be switched off.
    'process_inbound' => TRUE,
    // Turn off if url_outbound_alter hook is not to be run (e.g. to support some other module.)
    'process_outbound' => TRUE,
    // Config version (not version of the module!) see @todo below.
    'version' => 0,
    // Possibly have a version-specific url-prefix, but not vital as we can change 'fixes' above.
    //'version-prefix' => '';
  );

  return $vars;
}
