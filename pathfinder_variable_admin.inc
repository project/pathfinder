<?php
/**
 * Variable example.
 */

/**
 * Implements hook_menu().
 */
function pathfinder_menu() {
  $items['admin/config/search/path/pathfinder'] = array(
    'title' => 'PathFinder configuration',
    'description' => 'Configuration for the PathFinder module.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('variable_group_form', 'pathfinder_config'),
    'access arguments' => array('administer site configuration'),
  );
  return $items;
}
