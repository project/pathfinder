<?php

/*
 * @file
 * PathFinder - Short urls and future-proof url aliases.
 * PathFinder Redirect - Provides a simple redirect for use with PathFinder or independently.
 * Designed and developed by netgenius.co.uk.
 * Contact for commercial suppport and customisation: http://netgenius.co.uk/pathfinder
 *
 * This module provides general-purpose redirect from requested url to alias url if different.
 * It is not dependent on the main PathFinder module.
 */

/**
 * Implement hook_url_inbound_alter()
 * hook_url_inbound_alter(&$path, $original_path, $path_language)
 */
function pathfinder_redirect_url_inbound_alter(&$path, $original_path, $path_language) {
  static $busy = FALSE;

  // We need a flag to prevent possible recursion.
  if (!$busy) {
    $busy = TRUE;

    // Get current system path. We can't use current_path() as that does not invoke url alter hooks.
    $syspath = drupal_get_normal_path($path);
    //drupal_set_message('syspath: ' . $syspath . ' requested: '. $requested_path . ' language: ' . print_r($path_language));
    // If the path exists (not 404) then continue to check for needed redirect.
    if ($syspath && pathfinder_redirect_system_path_exists($syspath)) {
      // Get the path actually requested.  $path and $original_path do not indicate
      // a request to front-page, so we must check the original request.
      // For front-page we cannot use $_GET['q'] either, so must read request_uri().
      $requested_path = ltrim(parse_url(request_uri(), PHP_URL_PATH), '/');
      //drupal_set_message('req: ' . $requested_path);

      // Language processing - remove the language prefix if present...
      // See: http://api.drupal.org/api/drupal/includes%21locale.inc/function/locale_language_from_url/7
      if (is_callable('language_url_split_prefix')) {
        $url_part = variable_get('locale_language_negotiation_url_part', LOCALE_LANGUAGE_NEGOTIATION_URL_PREFIX);
        if ($url_part == LOCALE_LANGUAGE_NEGOTIATION_URL_PREFIX) {
          $split = language_url_split_prefix($requested_path, array($GLOBALS['language']));
          $requested_path = $split[1];
        }
      }
      pathfinder_redirect_main($syspath, $requested_path);
    }
    $busy = FALSE;
  }
}

// Main function. Check for and trigger a redirect if needed.
function pathfinder_redirect_main($syspath, $reqpath) {

  // Get the alias for the current path (without language).
  $alias = pathfinder_redirect_alias($syspath);
  // If the alias does not match the requested url, then we need to redirect to the alias.
  if ($alias != $reqpath) {
    // Only redirect if $_POST is empty, processing index.php and not under Drush
    if (empty($_POST) && $_SERVER['SCRIPT_NAME'] == '/index.php' && !function_exists('drush_main')) {
      // Get the Query String (minus the 'q').
      $get = $_GET; unset($get['q']);

      // Bypass if user has permission - show message instead of redirecting.
      $perm = 'bypass pathfinder redirect';
      if (function_exists('user_access') && user_access($perm)) {
        // Get the target path with language and query string so we can display it.
        $options = array('query' => $get);
        $url = ltrim(url($syspath, $options), '/');
        $text = ($syspath)? $url : sprintf('%s (%s)', $url, t('front'));
        $tvars = array(
          '%module' => 'PathFinder Redirect',
          '!link' => sprintf('<a href="/%s">%s</a>', $url, $text),
          '%perm' => $perm,
        );

        drupal_set_message(
          t('%module: redirect to !link (not done due to %perm permission.)', $tvars),
          'warning');
        /*
        $vars = array('alias', 'syspath', 'reqpath', 'url', 'text');
        foreach($vars as $var) {
          drupal_set_message(sprintf('%s: [%s]', $var, $$var), 'warning');
        }
        */
      }
      // Else, do the redirect.
      else {
        drupal_goto($syspath, $get, NULL, 301);
      }
    }
  }
}

// Test if path is current front-page.
function pathfinder_redirect_isfront($path, $path1 = TRUE, $path2 = FALSE) {
  $is_front = (!$path || $path == variable_get('site_frontpage', 'node'));
  return ($is_front)? $path1 : $path2;
}

// Get path alias without language and without leading slash.
function pathfinder_redirect_alias($path, $options = array()) {
  // Check for front-page.
  if (pathfinder_redirect_isfront($path)) {
    return '';
  }
  else {
    $language->language = LANGUAGE_NONE;
    $options['language'] = $language;
    return ltrim(url($path, $options), '/');
  }
}

/**
 * Implements hook_permission().
 */
function pathfinder_redirect_permission() {
  return array(
    'bypass pathfinder redirect' => array(
      'title' => t('Bypass PathFinder Redirect'),
      'description' => t('Disable redirect and display a message showing where the user would be redirected to.'),
    ),
  );
}

/**
 * Utility function to check if path is an existing system path.
 * We use menu_get_item(), there may be a better way.
 */
function pathfinder_redirect_system_path_exists($path) {
  return (menu_get_item($path) != FALSE);
}

// ---------------- End ----------------
