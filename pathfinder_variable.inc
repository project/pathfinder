<?php
/**
 * @file
 * Defines some variables for PathFinder.
 * PathFinder - Short urls and future-proof url aliases.
 * Designed and developed by netgenius.co.uk.
 * Contact for commercial suppport and customisation: http://netgenius.co.uk/pathfinder
 */

/**
 * Implements hook_variable_info().
 */
function pathfinder_variable_info($options) {

  if (module_exists('pathfinder_redirect')) {
    // Whether enable redirect.
    $vars['redirect'] = array(
      'title' => t('Redirect', array()),
      'default' => TRUE,
      'description' => t('If enabled, incoming requests are redirected to the current full URL.', array()),
      'element' => array(
        '#type' => 'checkbox',
      ),
    );
  }

  // Prefix/suffix strings for url building.
  $vars['fixes'] = array(
    'title' => t('Prefix and suffix', array()),
    'description' => t('Prefix and suffix for the short URL (two values separated by space).', array()),
  );

  // Formats, currently: simple, numeric, packed.
  $vars['format'] = array(
    'title' => t('URL format', array()),
    'default' => 'simple',
    'description' => t('Format for the short URL. Simple format is adequate for most sites.', array()),

    'element' => array(
      '#type' => 'radios',
      '#options' => array(
        'simple'  => t('Simple (e.g. node/1234 becomes n1234)'),
        'numeric' => t('Numeric (e.g. node/1234 becomes 3463)'),
        'packed'  => t('Packed (e.g. node/1234 becomes pLv)'),
      ),
    ),
  );

  $vars['dict_str'] = array(
    'title' => t('Dictionary', array()),
    'default' => 'bcdfghjklmnpqrtvwxzBCDFGHJKLMNPQRSTVWXZ0123456789',
    'description' => t('Characters which may be used to buld a packed format URL.', array()),
  );

  $vars['types_str'] = array(
    'title' => t('Types', array()),
    'default' => 'n|node c|comment t|taxonomy_term u|user',
    'description' => t(
      'Path types which will be processed as key|name pairs. The key is used in simple format URLs.
      Possible types are %node, %comment, %taxonomy_term and %user.
      Removing unwanted types will make your URLs slightly shorter.',
      array('%node' => 'node', '%comment' => 'comment', '%taxonomy_term' => 'taxonomy_term', '%user' => 'user')),
  );

  // Rebuild...
  foreach($vars as $key => $value) {
    $vars[$key]['group'] = 'pathfinder_config';
    $variables['pathfinder_' . $key] = $vars[$key];
  }
  return $variables;
}

/**
 * Implements hook_variable_group_info().
 */
function pathfinder_variable_group_info() {
  $groups['pathfinder_config'] = array(
    'title' => t('Examples'),
    'description' => t('Variable examples of different types.'),
    'access' => 'administer site configuration',
    'path' => array('admin/config/system/variable/example'),
  );
  return $groups;
}
